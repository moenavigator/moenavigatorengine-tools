#MoeNavigatorEngine-tools

MoeNavigatorEngine-tools is a collection of tools that use MoeNavigatorEngine
(or parts of it) to perform certain tasks related to XML and HTML data
conversion and extraction and for doing things over the network.


## Compilation

You need to compile MoeNavigatorEngine first since all those tools rely on it.
The source code of MoeNavigatorEngine with installation instructions can be
found here:

https://codeberg.org/moenavigator/moenavigatorengine

After you compiled MoeNavigatorEngine, you must make sure the linker can link
to the libmoenavigatorengine.so or libmoenavigatorengine.a library. The easiest
way to achieve this is to install MoeNavigatorEngine in your system using
"make install".


To compile MoeNavigatorEngine-tools, follow these steps:

1. Clone the source code from:
   https://codeberg.org/moenavigator/moenavigatorengine-tools

2. Go into the directory where you cloned the source file and invoke CMake:

    cmake -B build .

   This will create Makefiles in a new subdirectory called "build".

3. Enter the build directory and invoke make:

    make -jX

   X is the number of parallel jobs you want to execute.


After that, you have all the tools ready to be used inside the build directory.
If you want to install them in your system, you can run "make install".


## Documentation

The documentation is placed in the project's wiki at codeberg.org:

https://codeberg.org/moenavigator/moenavigatorengine-tools/wiki/index

You may also clone the whole wiki content with the source code as git submodule.
The wiki will then be available in the doc subdirectory of the
source code repository.
