/*
    This file is part of moenavigatorengine-tools
    Copyright (C) 2017-2018  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <iostream>
#include <memory>

#include <MoeNavigatorEngine/MoeNavigatorEngine.h>
#include <MoeNavigatorEngine/MNERenderer/MNERenderer_SVG.h>


int main(int argc, char** argv)
{
    std::string url = "";
    std::string output_file = "";
    if (argc == 1) {
        //Not enough arguments!
        std::cerr << "Usage: mne2svg URL [OUTPUT FILE]" << std::endl
            << "URL: An URL with a document that shall be converted into an SVG graphic." << std::endl
            << "OUTPUT FILE: An optional file where the SVG graphic shall be stored." << std::endl
            << "    If omitted the SVG graphic is written to stdout." << std::endl;
            return 0;
    } else if (argc > 1) {
        url = argv[1];
        if (argc > 2) {
            std::cerr << "WARNING: setting an output file is not yet supported!" << std::endl;
            output_file = argv[2];
        }
    }
    try {
        //create instance of MoeNavigatorEngine:
        std::shared_ptr<MoeNavigatorEngine> engine(
            new MoeNavigatorEngine()
        );
        std::shared_ptr<MNERenderer_SVG> renderer(
            new MNERenderer_SVG()
            );
        engine->setDocumentDimensions(800, 600); //Stub
        engine->addRenderer(renderer);
        engine->openURL(url);
        engine->drawPage();
        std::shared_ptr<MNERenderedData> data = renderer->getRenderedData();
        if (data != nullptr) {
            std::cout << data->data << std::endl;
        }
    } catch (MNE::Exception e) {
        std::cerr << e.toString() << std::endl;
    }

}
