/*
    This file is part of moenavigatorengine-tools
    Copyright (C) 2017-2018  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/




#include <fstream>
#include <iostream>
#include <memory>
#include <string>


#include <MoeNavigatorEngine/Network/NetworkHandler_POSIX.h>
#include <MoeNavigatorEngine/Network/NetworkHandler_GnuTLS.h>
#include <MoeNavigatorEngine/Network/HTTPRequest.h>
#include <MoeNavigatorEngine/Network/GopherRequest.h>
#include <MoeNavigatorEngine/Network/QOTDRequest.h>
#include <MoeNavigatorEngine/CommonClasses/URL.h>


void printHelp()
{
    std::cerr << "Usage: mneget [PARAMETERS] URL [OUTPUT FILE]" << std::endl
              << "PARAMETERS: One or more of the parameters listed below" << std::endl
              << "URL: An URL that shall be downloaded." << std::endl
              << "OUTPUT FILE: An optional file name for the URL's content." << std::endl
              << "    If omitted, the file will be written to stdout." << std::endl << std::endl;
    std::cerr << "Parameters:" << std::endl
              << "-q\tBe quiet: Do not output any text, just do the download." << std::endl
              << "--\tThe parameter following this one is the URL." << std::endl;
}


int main(const int argc, const char** argv)
{

    std::string url_str = "";
    std::string output_file = "";
    bool be_quiet = false;

    if (argc == 1) {
        //Not enough arguments!
        printHelp();
        return 0;
    } else if (argc > 1) {
        bool read_url = false;
        bool read_output_file = false;
        for (int i = 1; i < argc; i++) {
            std::string arg = argv[i];
            if (arg.length() > 0) {
                if (!read_url && !read_output_file && (arg[0] != '-')) {
                    //The argument does not start with a hyphen:
                    //It must be the URL.
                    read_url = true;
                }
                if (read_url) {
                    url_str = arg;
                    read_url = false;
                    read_output_file = true;
                } else if (read_output_file) {
                    output_file = arg;
                } else {
                    if (arg == "-q") {
                        be_quiet = true;
                    } else if (arg == "--") {
                        read_url = true;
                    } else {
                    }
                }
            }
        }

        if (url_str.empty()) {
            if (!be_quiet) {
                std::cerr << "Cannot continue. No URL provided!" << std::endl;
            }
            return 1;
        }
    }

    MNE::URL url(url_str);

    std::shared_ptr<MNE::Network::NetworkHandler_POSIX> network = nullptr;
    std::shared_ptr<MNE::Network::Request> request = nullptr;

    if ((url.protocol == "http" || url.protocol == "https")) {
        if (url.protocol == "https") {
            network = std::make_shared<MNE::Network::NetworkHandler_GnuTLS>();
        } else {
            network = std::make_shared<MNE::Network::NetworkHandler_POSIX>();
        }
        auto http_request = std::make_shared<MNE::Network::HTTPRequest>(
            network,
            url.domain,
            url.port,
            url.path
            );
        http_request->addRequestHeader("User-Agent", "mneget");
        http_request->start();

        auto response_header = http_request->getResponseHeader();

        //TODO: print some response data.

        request = http_request;
    } else if (url.protocol == "gopher") {
        network = std::make_shared<MNE::Network::NetworkHandler_POSIX>();
        auto gopher_request = std::make_shared<MNE::Network::GopherRequest>(
            network,
            url.domain,
            url.port,
            url.path
            );
        gopher_request->start();
        request = gopher_request;
    } else if (url.protocol == "qotd") {
        network = std::make_shared<MNE::Network::NetworkHandler_POSIX>();
        auto qotd_request = std::make_shared<MNE::Network::QOTDRequest>(
            network,
            url.domain,
            url.port,
            url.path
            );
        qotd_request->start();
        request = qotd_request;
    }

    if (request == nullptr) {
        if (!be_quiet) {
            std::cerr << "Unsupported protocol!" << std::endl;
        }
        return 1;
    }

    //Open the output file for writing:
    if (output_file.empty()) {
        //Write to stdout:
        std::ostream outstream(nullptr);
        outstream.rdbuf(std::cout.rdbuf());
        if (!outstream && !be_quiet) {
            std::cerr << "Cannot write to stdout!" << std::endl;
        }
        auto result = request->getResponseData();
        while (!result->empty()) {
            outstream.write((char*)result->data(), result->size());
            result = request->getResponseData();
        }
    } else {
        //Write to output file:
        auto outfile = std::ofstream(output_file, std::ofstream::binary);
        if (!outfile && !be_quiet) {
            std::cerr << "Cannot write to output file!" << std::endl;
            return 1;
        }
        auto result = request->getResponseData();
        while (!result->empty()) {
            outfile.write((char*)result->data(), result->size());
            result = request->getResponseData();
        }
    }
    if (!be_quiet) {
        std::cerr << "Download complete!" << std::endl;
    }
    return 0;
}
