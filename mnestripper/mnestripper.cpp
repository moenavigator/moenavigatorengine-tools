/*
    This file is part of moenavigatorengine-tools
    Copyright (C) 2017-2020  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <memory>

#include <MoeNavigatorEngine/MoeNavigatorEngine.h>
#include "./StrippedHtmlRenderer.h"


int main(int argc, char** argv)
{
    std::string url = "";
    std::string output_file = "";
    if (argc == 1) {
        //Not enough arguments!
        std::cerr << "Usage: mnestripper URL [OUTPUT FILE]" << std::endl
            << "URL: An URL that shall be stripped from unnecessary HTML" << std::endl
            << "OUTPUT FILE: An optional file where the stripped HTML shall be written into." << std::endl
            << "    If omitted the HTML is written to stdout." << std::endl;
            return 0;
    } else if (argc > 1) {
        url = argv[1];
        if (argc > 2) {
            std::cerr << "WARNING: setting an output file is not yet supported!" << std::endl;
            output_file = argv[2];
        }
    }
    try
    {
        //create instance of MoeNavigatorEngine:
        auto engine = std::make_shared<MoeNavigatorEngine>();

        //Create an instance of our renderer:
        auto renderer = std::make_shared<StrippedHtmlRenderer>();

        //Set our renderer to the engine:
        engine->addRenderer(renderer);

        //We must tell the engine which file we wish to open:
        engine->openURL(url);

        //std::cerr << "mnestripper: DEBUG: Engine has read the document." << endl;

        engine->drawPage();

        //When the engine has finished reading the input
        //it will process the input and build a document
        //node tree which is then passed to the renderer.
        //Our renderer will then return the rendered data
        //and we just have to print it to stdout.

        auto data = renderer->getRenderedData();

        //TODO: implement writing to the output file
        //instead of writing to stdout.
        std::cout << data->data << std::endl;
    } catch (MNE::Exception e) {
        std::cerr << e.toString() << std::endl;
        return 1;
    }

    return 0;
}
