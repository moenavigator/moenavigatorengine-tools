/*
    This file is part of moenavigatorengine-tools
    Copyright (C) 2017  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef STRIPPEDHTMLRENDERER_H
#define STRIPPEDHTMLRENDERER_H


#include <inttypes.h>

#include <MoeNavigatorEngine/MNERenderer/MNERenderer.h>


class StrippedHtmlRenderer: public MNERenderer
{
    protected:
        std::shared_ptr<DocumentNode> document;

        //The following array is for early development stage only.
        //It is incomplete! (as of 2017-06-18)
        std::vector<std::string> persistent_nodes;

        std::string output_document;

    public:
        StrippedHtmlRenderer();

        virtual void stripNodeContent(std::shared_ptr<DocumentNode> node);
        virtual void buildXHTMLDocument();

        //MNERenderer interface implementation:

        /**
         * Returns the output modes of this renderer.
         */
        virtual uint32_t getSupportedOutputModes();

        virtual void setOutputMode(MNERenderer::OutputMode mode);

        /**
         * The StrippedHtmlRenderer does not draw anything.
         * Therefore calling this method does not have any effect.
         */
        virtual void setDrawingAreaDimensions(
            uint32_t width,
            uint32_t height
            );
        virtual void setDocument(std::shared_ptr<DocumentNode> document);

        /**
         * Returns the rendered data (the stripped HTML document).
         */
        virtual std::shared_ptr<MNERenderedData> getRenderedData();

        virtual void render();
};

#endif
